console.log('console is working')


let countryarray = [];
function buttonClickHandler()
{

    //making an xhr object//
    const xhr = new XMLHttpRequest();

    //opening the object//
    xhr.open('GET', 'https://restcountries.eu/rest/v2/all', true);

    //when it is under process the functioning will be v //

    xhr.onprogress = function(){
        console.log('In progress');
    }
    // functioning when everything is ready//

    xhr.onload = function() {  

    let countryobj = JSON.parse(this.responseText);
    countryarray = JSON.parse(this.responseText);
    console.log(countryobj);
    appendData(countryobj);
  
    }
    xhr.send()
    console.log("We are done")
  }
    

    function appendData(countryobj) 
    {

        let card = document.getElementById('card');

        

        let str1 = "";
        
        
        
        for ( let key in countryobj) {

      str1 += `<div class="row justify-content-center">
      <div class="col-md-6">
        <div class="d-flex flex-column">
        
        
          <div class="box row my-2">
            <div class="col-xl-4 py-2">
              <img
                class="img-fluid border border-dark"
                id="countryflag"
                src="${countryobj[key].flag}"
                alt=""
              />
            </div>
            <div class="col-xl-8 mx-auto my-auto">
              <div class="row font-weight-bold">
                <div class="col-xl-8"><h5>${countryobj[key].name}</h5></div>
              </div>
              <div class="row pt-1">
                <div class="col-xl-8"><h7><b>Currency</b> :${countryobj[key].currencies[0].name} </h7></div>
              </div>
              <div class="row pt-1">
                <div class="col-xl-12"><h7><b>Capital</b> :${countryobj[key].capital}</h7></div>
              </div>
              <div class="row pt-3">
                <div class="col-sm-6">
                  
                  <a href="https://www.google.co.in/maps/place/${countryobj[key].name}/" target="_blank"><button type="button" class="btn btn-lg  btn-outline-primary afgbtn" >Show Map</button> </a>
                 
                  </div>
                  <div class = "col-sm-6">
                 
                    <a href="detail.html" class = " btn btn-lg btn-outline-primary" >
                    <button type="button" class = " btn2 " onClick="detailbtn(this.value)" value= "${countryobj[key].alpha3Code}">Details</button>
                    </a>
                  
                  </div>                
              </div>
            </div>
          </div>
         </div>
         </div>
        </div>
        </div>`;


        }
        
        card.innerHTML = str1;
    }

    //sending the data//
    




function searchfun()
    {

        let filter = document.getElementById('myInput').value.toUpperCase();
        console.log(filter);
        //console.log(countryarray);

        let resultdata=countryarray.filter((country)=>{
        return country.name.toUpperCase().startsWith(filter);
        });
        appendData(resultdata)

        

    }


function detailbtn(val){
    let countryDetail = val;
    console.log(countryDetail);
    window.localStorage.setItem('country', val);
}