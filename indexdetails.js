console.log('working')

 

let selectedcountry = window.localStorage.getItem('country');
console.log(selectedcountry);



async function info(url)
{
    //fetching the data
    const responsecoming = await fetch(url);
    //parsing the data to json fomrmat
    seeinfo = await responsecoming.json();
    //if responsecoming is true
    if(responsecoming){
        console.log('we are getting the info')
        console.log(seeinfo)
    }
    console.log("fetched the info")
    showcountryflags(seeinfo);
    showdetails(seeinfo);
    console.log(seeinfo)   
}



const apiurl = `https://restcountries.eu/rest/v2/alpha/${selectedcountry}`
info(apiurl);

function showcountryflags(cdata)
{
    const countryflagbox = [];

    for(border in cdata.borders){
        const flag = 'https://restcountries.eu/data/'+ cdata.borders[border].toLowerCase() + '.svg'
        countryflagbox.push(flag);
    }
    console.log(countryflagbox)

    let countryf = ''
    let onecountryflag;

    for(onecountryflag in countryflagbox){
        


        countryf += `
        <div class="col-md-4 col-sm-12 col-xl-4">
           <div class="row"><img class="img-fluid" src="${countryflagbox[onecountryflag]}"></div>
        </div>`;
        
        
    }
    
    document.getElementById("flags").innerHTML = countryf;
}


function showdetails(seeinfo){

    document.getElementById('detail').innerHTML = `
    <div class="row">
    <div class="col-sm-12">
        <h3 class= "font-weight-bold pt-3">${seeinfo.name}</h3>
    </div>
    </div>
        
    <div class="row">
    <div class=" col-sm-12 col-md-4 col-lg-4">
        <img src="${seeinfo.flag}" class="img-fluid " alt="india">
    </div>
    <div class=" col-sm-12 col-md-8 col-lg-8">
        <div><h6>Native Name: ${seeinfo.name}</h6></div>
        <div><h6>Capital: ${seeinfo.capital}</h6></div>
        <div><h6>Population: ${seeinfo.population}</h6></div>
        <div><h6>Region:${seeinfo.region} </h6></div>
        <div><h6>Sub-region: ${seeinfo.subregion}</h6></div> 
        <div><h6>Area: ${seeinfo.area}</h6></div>
        <div><h6>Country Code: ${seeinfo.callingCodes}</h6></div>
        <div><h6>Languages: ${seeinfo.languages[0].name}</h6></div>
        <div><h6>Currencies: ${seeinfo.currencies[0].name}</h6></div>
        <div><h6>Timezones: ${seeinfo.timezones}</h6></div>
     </div>
</div>`
}
